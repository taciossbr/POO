public class Pessoa {
    private String cpf;
    private String nomePessoa;
    private String endereco;

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }
    public String getCpf() {
        return this.cpf;
    }
    public void setNomePessoa(String nome) {
        this.nomePessoa = nome;
    }
    public String getNomePessoa() {
        return this.nomePessoa;
    }
    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }
    public String getEndereco() {
        return this.endereco;
    }
}